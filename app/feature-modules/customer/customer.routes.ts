import Router, { NextFunction, Request, Response } from "express"
import { ResponseHandler } from "../../utility/response.handler";
import { permit } from "../../middleware/permit";
import { ROLE } from "../roles/role.types";
import { CREATE_CUSTOMER_VALIDATION } from "./customer.validations";
import customerServices from "./customer.services";

const router = Router()

router.post('/create', CREATE_CUSTOMER_VALIDATION, permit([ROLE.ADMIN]), async(req:Request, res:Response, next:NextFunction) => {
    try{
        const meter = req.body;
        const result = await customerServices.create(meter)
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
})


// router.get('/my-customers/:employeeId',permit([ROLE.EMPLOYEE]),async(req:Request, res:Response, next:NextFunction) =>{
//     try{
//         const {employeeId} = req.params
//         const result = await customerServices.findAllCustomers({ employeeId : employeeId})
//         res.send(new ResponseHandler(result))
//     }catch(e){
//         next(e)
//     }
// })

router.get('/my-customers',permit([ROLE.EMPLOYEE]),async(req:Request, res:Response, next:NextFunction) =>{
        try{
            const {id} = res.locals.tokenDecode
            const result = await customerServices.findAllCustomers({ employeeId : id})
            res.send(new ResponseHandler(result))
        }catch(e){
            next(e)
        }
    })
    

router.get('/customers-in-meter/:meterId',permit([ROLE.ADMIN]),async(req:Request, res:Response, next:NextFunction) =>{
    try{
        const {meterId} = req.params
        const result = await customerServices.findAllCustomers({ meterId : meterId})
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
})

router.get('/:customerId',permit([ROLE.EMPLOYEE,ROLE.ADMIN]),async(req:Request, res:Response, next:NextFunction) =>{
    try{
        const {customerId} = req.params
        const result = await customerServices.findById(customerId)
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
})

router.get('/',permit([ROLE.ADMIN]),async(req:Request, res:Response, next:NextFunction) =>{
    try{
        const result = await customerServices.find()
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
})

export default router