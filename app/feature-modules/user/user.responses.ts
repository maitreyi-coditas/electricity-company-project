export const USER_RESPONSE = {

    NOT_FOUND : {
        statusCode: 404,
        message : "Not Found"
    },
    UNAUTHORISED_ACCESS : {
        statusCode: 401,
        message : "Unauthorised Access"
    },
    UPDATE_FAILED : {
        statusCode: 400,
        message : "User not updated"
    },

    UPDATE_SUCCESSFULL : {
        statusCode: 200,
        message : "User updated succesfully"
    },

    DELETE_FAILED : {
        statusCode: 400,
        message : "User not updated"
    },

    DELETE_SUCCESSFULL : {
        statusCode: 200,
        message : "User deleted succesfully"
    },

}