import { IsCreditCard } from "express-validator/src/options"
import customerRepo from "./customer.repo"
import { ICustomer } from "./customer.types"

const create = (customer : ICustomer) => customerRepo.create(customer)

const find = () => customerRepo.find()

const findAllCustomers = (filter : Partial<ICustomer>) => customerRepo.findAllCustomers(filter)

const findById = (id : string) => customerRepo.findById(id)

export default {
    create,
    find,
    findAllCustomers,
    findById
}