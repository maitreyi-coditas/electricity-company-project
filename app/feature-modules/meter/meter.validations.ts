import { body } from "express-validator";
import { validate } from "../../utility/validate";

export const CREATE_METER_VALIDATION = [

    body("meterType").isString().notEmpty().withMessage("Meter type is required"),
    body("ratePerUnit").isNumeric().notEmpty().withMessage("Rate per unit is required"),
    body("photosRequired").isNumeric().notEmpty().withMessage("Photos required is required"),
    validate
]