export interface IBill {

    customerId:string,
    totalReading : number,
    totalAmount? : number,
    dueAmount?: number,
    paymentstatus?:string,
    photos:IPhoto[],

}

export interface IPhoto {
        filename : string, 
        data : string,
        contentType? : string
}

