import { IBill } from "../bill/bill.types";

export interface ICustomer {

    name:string,
    email:string,
    address:string,
    meterId:string,
    employeeId:string,
    billDetails?:IBill[]

}

