import userRouter from "./user/user.routes"
import roleRouter from "./roles/role.routes"
import authRouter from "./auth/auth.routes"
import meterRouter from "./meter/meter.routes"
import customerRouter from "./customer/customer.routes"

import billRouter from "./bill/bill.routes"

export default { userRouter, roleRouter,authRouter, meterRouter,customerRouter,billRouter }