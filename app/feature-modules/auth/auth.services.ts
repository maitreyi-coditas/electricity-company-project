import { compare, genSalt, hash } from "bcryptjs";
import { IUser } from "../user/user.types";
import userServices from "../user/user.services";
import { ROLE } from "../roles/role.types";
import { ICredentials } from "./auth.types";
import { AUTH_RESPONSES } from "./auth.responses";
import jwt, { sign } from "jsonwebtoken";
import fs from "fs";
import path from "path";

const encryptUserPassword = async (user : IUser) => {

    const salt = await genSalt(10)
    const hashedPassword = await hash(user.password,salt)
    user.password = hashedPassword;

    return user
}
  
const register = async(user : IUser) => {

    user = await encryptUserPassword(user)

        if(!user.role){
        user.role = ROLE.EMPLOYEE
        }

        const record = await userServices.create(user) 
        return record
}

const login = async (credentials: ICredentials) => {

    const user = await userServices.findOne({ email : credentials.email})

    if(!user) throw AUTH_RESPONSES.INVALID_CREDENTIALS

    const isPasswordValid = await compare(credentials.password,user.password)

    if(!isPasswordValid) throw AUTH_RESPONSES.INVALID_CREDENTIALS


    const { _id,role } = user;


    const PRIVATE_KEY = fs.readFileSync(path.resolve(__dirname,"..\\..\\keys\\private.pem" ), {encoding : "utf-8"})

    try {
        //payload, private key, algorithm
        var token = jwt.sign({id : _id , role : role},  PRIVATE_KEY || "", {algorithm:'RS256'});

        return {token};

    } catch (error) {
        console.log(error)
    }
   
    // SYMMETRIC KEY
    // const { JWT_SECRET } = process.env;

    // const token = sign({id : _id, role : role}, JWT_SECRET || '')
    // console.log(token)
    // return { token };

};


export default {
    register,
    login
}