import { Schema, model } from "mongoose";
import { BaseSchema } from "../../utility/base-schema";
import { ICustomer } from "./customer.types";

const CustomerSchema = new BaseSchema({

    name:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    address:{
        type:String,
        required:true
    },
    meterId:{
        type: Schema.Types.ObjectId,
        ref:"Meter",
        required:true
    },
    employeeId:{
        type: Schema.Types.ObjectId,
        ref:"User",
        required:true  
    },
    billDetails: {
        type: [{
            _id : {
                type : Schema.Types.ObjectId,
                required:true,
            },
            totalAmount : {
                type: Number,
                required:true
            },
            dueAmount : {
                type: Number,
                required:true   
            },
            paymentStatus : {
                type: Number,
                required:true  
            },
            photos :{
                type:[String],
                required:true
            }
        }]
    }
})

type CustomerDocument = Document & ICustomer

export const customerModel = model<CustomerDocument>("Customers",CustomerSchema)

// {
//     photolist : {
//         type : [String],
//         required:true
//     },
//     reason : {
//         type : String,
//         required:false 
//     }
// }