import { body } from "express-validator";
import { validate } from "../../utility/validate";

export const CREATE_CUSTOMER_VALIDATION = [
    body("name").isString().notEmpty().withMessage("Name is required"),
    body("email").isString().notEmpty().withMessage("Email is required"),
    body("address").isString().notEmpty().withMessage("Address is required"),
    body("meterId").isString().notEmpty().withMessage("MeterId is required"),
    body("employeeId").isString().notEmpty().withMessage("EmployeeId is required"),
    validate
]

