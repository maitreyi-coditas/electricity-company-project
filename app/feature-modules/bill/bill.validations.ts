import { body } from "express-validator";
import { validate } from "../../utility/validate";

export const CREATE_BILL_VALIDATION = [
    body("customerId").isString().notEmpty().withMessage("Customer Id is required"),
    body("totalReading").isNumeric().notEmpty().withMessage("Total Reading is required"),
    body("photos").isArray().notEmpty().withMessage("Photos is required"),
    validate
]

