import Router, { NextFunction, Request, Response } from "express"
import { ResponseHandler } from "../../utility/response.handler";
import { permit } from "../../middleware/permit";
import { ROLE } from "../roles/role.types";
import { CREATE_BILL_VALIDATION } from "./bill.validations";
import billServices from "./bill.services";

const router = Router()

router.post('/create', CREATE_BILL_VALIDATION, permit([ROLE.EMPLOYEE]), async(req:Request, res:Response, next:NextFunction) => {
    try{
        const {id} = res.locals.tokenDecode;
        const bill = req.body;
        const result = await billServices.create(bill,id)
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
})

router.post('/pay-bill', permit([ROLE.ADMIN]), async(req:Request, res:Response, next:NextFunction) => {
    try{
        const {id} = res.locals.tokenDecode;
        const billId= req.body;
        const result = await billServices.payBill(billId)
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
})



export default router