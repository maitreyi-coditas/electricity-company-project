import { Schema, model } from "mongoose";
import { BaseSchema } from "../../utility/base-schema";
import { IBill } from "./bill.types";

const BillSchema = new BaseSchema({

    customerId: {
        type: Schema.Types.ObjectId,
        ref:'Customers',
        required:true
    },
    // currentTotalReading : {
    //     type : Number,
    //     required : true
    // },
    totalAmount : {
        type : Number,
        required : true
    },
    dueAmount : {
        type : Number,
        required : true
    },
    paymentstatus : {
        type : String,
        required : true
    },
    photos: {
        type : [{
            filename: { 
                type: String, 
                required: true 
            },
            contentType: { 
                type: String, 
                required: true 
            },
            data: { 
                type: Buffer, 
                required: true 
            },
        }]
    }


})

type billDocument =  Document & IBill

export const billModel = model<billDocument>("Bills",BillSchema)


// customerId:string,
// totalReading : number,
// totalAmount? : number,
// dueAmount?: number,
// paymentstatus?:string,
// photos:[{
//     filename : string, 
//     data : string
// }],



