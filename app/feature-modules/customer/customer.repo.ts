import { customerModel } from "./customer.schema";
import { ICustomer } from "./customer.types";

const create = (customer : ICustomer) => customerModel.create(customer)

const find = () => customerModel.find()

const findAllCustomers = (filter : Partial<ICustomer>) => customerModel.find(filter)  

const findById = (id : string) => customerModel.findById(id)

export default {
    create,
    find,
    findAllCustomers,
    findById
}