import  mongoose  from "mongoose";
import { ObjectId } from "bson";

export interface IRole {

    _id? : ObjectId,
    name : string
}

export type Role = IRole[]    

export const ROLE_IDS = {
    ADMIN : new mongoose.mongo.ObjectId("641eec5129e5299e589c9cb1"),
    EMPLOYEE : new mongoose.mongo.ObjectId("641eec5929e5299e589c9cb3"),
   
}

export const ROLE = {
    ADMIN: ROLE_IDS.ADMIN.toString(),
    EMPLOYEE: ROLE_IDS.EMPLOYEE.toString(),
}

// export type rolePredicate = (cb : IRole) => boolean