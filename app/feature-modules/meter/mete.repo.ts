import { meterModel } from "./meter.schema";
import { IMeter } from "./meter.types";


const create = (meter : IMeter) => meterModel.create(meter)

const findById = (id : string) => meterModel.findById(id) 

export default {
    create,
    findById
}