import { FilterQuery, Types, UpdateQuery } from "mongoose";
import { UserModel } from "./user.schema";
import { IUser } from "./user.types";

const create = (user: IUser) => UserModel.create(user);

const findAll = () => UserModel.find({ isDeleted : false})

const findOne = async(filter : Partial<IUser>) => {

        try{

           return await UserModel.findOne({
                ...filter,
                isDeleted : false
            })

        }catch(e){
            throw { message : "Something went wrong! Not able to find the user"}
        }
}

const update = async(user : IUser) => {

    try {
        return await UserModel.updateOne({ _id : new Types.ObjectId(user._id) }, user
    );
    }catch(e){
        throw { message : "Something went wrong! Not able to update the user"}
    }

}
const findOneAndUpdate = async(filter : Partial<IUser>,query : Partial<IUser>) => {
    try{
        return await UserModel.findOneAndUpdate(filter,query)
    }catch(e){
        throw { message : "Something went wrong! Not able to find and update the user"}
    }
}

const deleteOne = async(id : string) => {
    
    try {
        return await UserModel.updateOne({ _id : new Types.ObjectId(id) }, {isDeleted: true});
    }catch(e){
        throw { message : "Something went wrong! Not able to delete the user"}
    }
} 

export default{
    create,
    findAll,
    findOne,
    update,
    findOneAndUpdate,
    deleteOne

}