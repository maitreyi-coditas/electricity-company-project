import mongoose from "mongoose"

export interface IMeter {
    meterType:string,
    ratePerUnit:number,
    photosRequired:number
}

export const METER_IDS = {
    NORMAL: new mongoose.mongo.ObjectId("641ef3fccc73acd87558da55"),
    COMMERCIAL : new mongoose.mongo.ObjectId("641ef3e4cc73acd87558da53"),
    SOLAR : new mongoose.mongo.ObjectId("641ef3b9cc73acd87558da51"),
}

export const METER = {
    NORMAL: METER_IDS.NORMAL.toString(),
    COMMERCIAL: METER_IDS.COMMERCIAL.toString(),
    SOLAR: METER_IDS.SOLAR.toString()
}