import { Route, Routes } from "./routes.types";
import routeProvider from "../feature-modules/route-provider";
import { ExcludedPath, ExcludedPaths } from "../middleware/authorize";

export const routes : Route = [

    new Routes('/role',routeProvider.roleRouter),
    new Routes('/auth',routeProvider.authRouter),
    new Routes('/users',routeProvider.userRouter),
    new Routes('/meter',routeProvider.meterRouter),
    new Routes('/customer',routeProvider.customerRouter),
    new Routes('/bill',routeProvider.billRouter)
]   


export const excludedPaths: ExcludedPaths = [
    new ExcludedPath('/role/create',"POST"),
    new ExcludedPath("/auth/login", "POST"),

];
