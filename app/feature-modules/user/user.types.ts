export interface IUser {

    _id?:string,
    name:string,
    email:string,
    password:string,
    role:string
}

export type User = IUser[]