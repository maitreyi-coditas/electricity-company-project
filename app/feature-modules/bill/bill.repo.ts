import { billModel } from "./bill.schema";
import { BILL_STATUS } from "./bill.status";
import { IBill } from "./bill.types";

const create = (bill : IBill) => billModel.create(bill)

const findAllBills = (filter: Partial<IBill>) => billModel.find(filter)

const payBill = (filter: Partial<IBill>) => billModel.updateMany(filter, {
    $set:{
        dueAmount : 0,
        paymentstatus: BILL_STATUS.PAID
    }
})



export default {
    create,
    findAllBills,
    payBill
}

