import { Request, Response, NextFunction, Router } from "express";
import roleServices from "./role.services";
import { ResponseHandler } from "../../utility/response.handler";
import { CREATE_ROLE_VALIDATION } from "./role.validation";

const router = Router()

router.post("/create",CREATE_ROLE_VALIDATION, async (req: Request,res : Response,next : NextFunction) => {

    try{
        const result = await roleServices.create(req.body);
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
    
})

export default router;