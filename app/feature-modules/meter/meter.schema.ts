import { model } from "mongoose";
import { BaseSchema } from "../../utility/base-schema";
import { IMeter } from "./meter.types";

const MeterSchema = new BaseSchema({

    meterType : {
        type: String,
        required:true
    },
    ratePerUnit : {
        type : Number,
        required : true
    },
    photosRequired : {
        type : Number,
        required : true
    },

})

type meterDocument =  Document & IMeter

export const meterModel = model<meterDocument>("Meter",MeterSchema)


