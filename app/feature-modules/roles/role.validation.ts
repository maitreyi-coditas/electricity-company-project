import { body } from "express-validator";
import { validate } from "../../utility/validate";

export const CREATE_ROLE_VALIDATION = [

    body("name").isString().notEmpty().withMessage("Name of the Role is required"),
    validate
]

 

