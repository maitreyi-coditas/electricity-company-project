import { CUSTOMER_RESPONSES } from "../customer/customer.responses";
import customerServices from "../customer/customer.services";
import { METER_RESPONSES } from "../meter/meter.responses";
import meterServices from "../meter/meter.services";
import { USER_RESPONSE } from "../user/user.responses";
import billRepo from "./bill.repo";
import { BILL_STATUS } from "./bill.status";
import { IBill } from "./bill.types";

const create = async(bill : IBill,employeeId : string) => {

   const customer = await customerServices.findById(bill.customerId)
   if(!customer) throw CUSTOMER_RESPONSES.NOT_FOUND

   if(customer.employeeId.toString() === employeeId){

    const meter =  await meterServices.findById(customer.meterId.toString())
    if(!meter) throw METER_RESPONSES.NOT_FOUND

    const currentBillAmount = bill.totalReading * meter.ratePerUnit
    console.log(currentBillAmount)

    const previousBills = await findAllBills({ customerId : bill.customerId })

    if(previousBills.length > 0){
        let previousDueAmount = 0
        previousBills.forEach(previousBill => {
            if(previousBill.paymentstatus === BILL_STATUS.PENDING){
                if(!previousBill.dueAmount){
                    previousDueAmount += 0
                }else{
                    previousDueAmount += previousBill.dueAmount
                }
            }
        })
        console.log(previousDueAmount)
        bill.totalAmount = currentBillAmount + previousDueAmount
        bill.dueAmount =  bill.totalAmount
        console.log(bill.totalAmount)
    }else{
        bill.totalAmount = currentBillAmount
        bill.dueAmount = currentBillAmount
    }


    if(bill.photos.length === meter.photosRequired){
        bill.photos.map(bill => {
            const buffer = Buffer.from(bill.data, 'base64')
            bill.data = buffer.toString()
            bill.contentType = 'image/png'
        })
    }else{
        throw ({ message : "Not enough photos"})
    }

    bill.paymentstatus = BILL_STATUS.PENDING

    return await billRepo.create(bill)
   
}
   else{
    throw USER_RESPONSE.UNAUTHORISED_ACCESS
   }

}

const findAllBills = (filter: Partial<IBill>) => billRepo.findAllBills(filter)

const payBill = (filter: Partial<IBill>) => billRepo.payBill(filter)



export default {
    create,
    findAllBills,
    payBill
}