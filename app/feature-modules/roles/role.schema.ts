import { model, Document } from "mongoose";
import { BaseSchema } from "../../utility/base-schema";
import { IRole } from "./role.types";

export const roleSchema = new BaseSchema({

    name : {
        type : String,
        required : true
    }
})

type roleDocument = Document & IRole;

export const roleModel = model<roleDocument>("Role",roleSchema);