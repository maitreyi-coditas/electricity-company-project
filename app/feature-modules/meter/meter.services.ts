import meteRepo from "./mete.repo"
import { IMeter } from "./meter.types"

const create = (meter : IMeter) => meteRepo.create(meter)


const findById = (id : string) => meteRepo.findById(id) 

export default {
    create,
    findById
}