import { Schema, model } from "mongoose";
import { BaseSchema } from "../../utility/base-schema";
import { IUser } from "./user.types";


const userSchema = new BaseSchema ({

    name : {
        type : String,
        required: true
    },

    email : {
        type : String,
        required: true,
        unique:true
    },

    password : {
        type : String,
        required: true
    },

    role : {
        type : Schema.Types.ObjectId,
        required : true,
        ref:"Role"
    },

})

export type UserDocument = Document & IUser

export const UserModel = model('User',userSchema)
