import Router, { NextFunction, Request, Response } from "express"
import meterServices from "./meter.services";
import { ResponseHandler } from "../../utility/response.handler";
import { CREATE_METER_VALIDATION } from "./meter.validations";
import { permit } from "../../middleware/permit";
import { ROLE } from "../roles/role.types";

const router = Router()

router.post('/create', CREATE_METER_VALIDATION, permit([ROLE.ADMIN]), async(req:Request, res:Response, next:NextFunction) => {
    try{
        const meter = req.body;
        const result = await meterServices.create(meter)
        res.send(new ResponseHandler(result))
    }catch(e){
        next(e)
    }
})

export default router